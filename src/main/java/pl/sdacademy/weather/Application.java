package pl.sdacademy.weather;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.client.RestTemplate;
import pl.sdacademy.weather.service.UserInterface;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("pl.sdacademy.weather")
@PropertySource("application.properties")
public class Application {

    public static void main(String[] args) {
        System.out.println("Hello, Weather!");

        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        context.getBean(UserInterface.class).start();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setBasename("classpath:messages");
        messageSource.setFallbackToSystemLocale(false);
        return messageSource;
    }
}