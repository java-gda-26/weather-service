package pl.sdacademy.weather.service;

        import lombok.RequiredArgsConstructor;
        import org.aspectj.lang.ProceedingJoinPoint;
        import org.aspectj.lang.annotation.Around;
        import org.aspectj.lang.annotation.Aspect;
        import org.aspectj.lang.annotation.Pointcut;
        import org.springframework.stereotype.Component;
        import pl.sdacademy.weather.dto.WeatherData;

@RequiredArgsConstructor
@Aspect
@Component
public class WeatherDataCacheAspect {

    private final WeatherDataCache cache;

    @Pointcut("@annotation(pl.sdacademy.weather.service.WeatherDataCached)")
    public void cacheAnnotation() {
    }

    @Pointcut("args(java.lang.String, ..)")
    public void hasStringAsFirstArgument() {
    }

    @Around("cacheAnnotation() && hasStringAsFirstArgument()")
    public Object handleCache(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] joinPointArgs = joinPoint.getArgs();
        String cityName = (String) joinPointArgs[0];
        WeatherData cachedData = cache.find(cityName);
        if (cachedData != null) {
            System.out.println("Data from cache");
            return cachedData;
        }

        System.out.println("Data from API");
        WeatherData weatherData = (WeatherData) joinPoint.proceed();

        cache.save(cityName, weatherData);
        return weatherData;
    }
}
