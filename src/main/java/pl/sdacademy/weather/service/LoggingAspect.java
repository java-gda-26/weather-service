package pl.sdacademy.weather.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Pointcut("@annotation(pl.sdacademy.weather.service.ExecutionTimeLog)")
    public void handleLoggingAnnotation() {
    }

    @Around("handleLoggingAnnotation()")
    public Object handleLogging(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.nanoTime();

        String city = (String) joinPoint.getArgs()[0];

        Object o = joinPoint.proceed();
        System.out.println("Execution time: " + (System.nanoTime() - startTime) + " for city: " + city);
        return o;
    }
}